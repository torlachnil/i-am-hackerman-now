# Get base Ubuntu image
FROM kalilinux/kali-rolling
# Update apps on base image and install main tools
# Install packages
RUN apt-get update && DEBIAN_FRONTEND=noninteractive \
                      apt-get install -y --fix-missing kali-linux-large

#RUN apt-get install -y software-properties-common kali-linux-full --fix-missing && \
#    echo 'VERSION_CODENAME=kali-rolling' >> /etc/os-release
RUN apt-get update && apt-get install -y \
                procps \
                nano \
                sudo \
                net-tools \
                wireless-tools \
                iputils-ping \
                dialog \
                ftp \
                build-essential \
                checkinstall \
                python3 \
                python3-pip \
                git \
                apache2 apache2-utils \
                ssh \
                postgresql \
                nmap \
                theharvester \
                whatweb \
                metasploit-framework \
                nikto \
                netdiscover \
                exploitdb \
                exploitdb-papers \
                exploitdb-bin-sploits \
                firefox-esr \
                gobuster \
                dos2unix

# Add conf for apache & friends. Assuming that port 8080 is used in docker-compose.yaml
RUN echo 'ServerName localhost' >> /etc/apache2/apache2.conf && \
    echo 'Listen 8080' >> /etc/apache2/apache2.conf && \
    echo "source /usr/src/hackerman/env.sh" >> /root/.bashrc

RUN pip3 install pyftpdlib

# Start services at build
RUN service ssh start && \
    service postgresql start

# Install git packages
WORKDIR /opt/
RUN git clone https://github.com/SecureAuthCorp/impacket.git
WORKDIR /opt/impacket
RUN pip3 install .
WORKDIR /usr/src/hackerman

# Set up VPN infrastructure
RUN mkdir -p /dev/net && \
    mknod /dev/net/tun c 10 200 && \
    chmod 600 /dev/net/tun