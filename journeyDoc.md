# Ethical Hacking Course

I work through the course https://www.youtube.com/watch?v=3Kq1MIfTWCE&list=WL 

The way to go for pentesting/hacking is apparently the Linux distribution kalled Kali-Linux. 
Since I am on a Windows computer, and I really like Docker, I will go the way of a Kali-Linux Docker
container with what I hope is a fairly slim build. If I am really lucky, I can do some (ethical) hacker
stuff without ever launching the container in privileged mode, or at most only giving it access to 
some net/bluetooth drivers. This may or may not be good for safety, as there is a clear limit to
what can be done to my computer from the container I expose to the web. At least that is the fantasy.

* At some point I will have to give the container some priveleges, or figure out how to bind a VPN tunnel on my host machine to a docker freindly address. I will probably also have to setup an X-server for some GUI apps, although I would prefer to avoid it. 

* To access hackthebox, I need an openVpn configuraiton file from hackthebox. I am not uploading this to
the public git repo. Gotta redownload to do hacktheboxes.

To maintain my bash history as I destory and rebuild my Linux container, I mount a local volume in
the current repo that can be accessed from the kali container. Bash stores its history in the file
 .bash_history (using "hist.txt" and "env.sh"). Basically, my dockerfile does
```
RUN echo "source /usr/src/hackerman/env.sh" >>  /root/.bashrc
```
so that whenever I run attachToContainer.ps1 from my powershell, env.sh is sourced, which runs
```
cat /usr/src/hackerman/hist.txt > ~/.bash_history
history -r
alias exit="history > /usr/src/hackerman/hist.txt && exit"
```

In the second lesson, some python basics are covered (that I mostly know) and the scripts
in /week2/ called bof.py and sweep.py are shown. I tried to modify them so that they funciton from 
within the docker container. The main problem was understanding how Pipes worked and that Pipes
hanging was a very common problem to debug when using the subprocess module =)

bof.py is a form of fuzzer that sends packets of increasing size until the connection is rejected.
In general, a fuzzer is a program that tries to feed another program with random (or pseudorandom) input until it breaks or reacts in a strange way. Sweep.py is roughly the same thing as the ipsweep.sh script from /week1/, which pings all IP adresses under some submask (i.e. 192.168.1) and tells you which ones responded. 

Python has an inbuilt module called "SimpleHTTPServer" in python 2 and "http.server" in python 3.
We can use this to publish files to the internet easily (very practical if you managed to get a shell from some target computer and want to extract files.) To initiate the server, do
```
python3 -m http.server <port>
```
To actually access anything that I publish from the Docker on my host machine, I go via localhost.
In the dockerfile, the fields
```
 ports:
      - 8080:8080
```
tell the docker container to map port 8080 in the container to port 8080 on the host machine. For anything to be accessible to the host, it shall be published on port 8080, and the host shall look for it on port 8080. 

In the folder /week3/ we wrote a basice, single threaded port scanner. It tries to establish a connection to some IP on a set of ports, tells us which ports worked.

### Actual hacking - five stages

#### Reconnaissance

Gether information about target. "Step 1" may be considered passive in that we are not interacting directly with the target server, but rather using information that is publically available to get information about the target company or other server owning entity.

**Some key stuff:**

Location information - satellite images, drone recon, building layout (badge readers, break areas, security, fencing, are people leaving doors open?)

Job information: Employees (names, jobs, titles, phone numbers, managers etc.), pictures(badge photos, desk photos, computer photos etc.)

A suprising amount of these things are on linkedin/facebook (in public!). Pictures of computers show what office suites are used, slack channel names, accounting information. Badge pictures make badge-cloning very easy. 

There is a 2k$ tool that can capture an RFID and clone it. You can in principle build it yourself for about 500$, and apparently the instrucitons are available. 

Passive web/host recon: 
* Target validation: WHOIS, nslookup, dnsrecon 
    * It would be really bad if a customer gave you an IP that wasn't their own and you started deploying explots... 
* Finding subdomains: Google Fu, dig, Nmap, Sublist3r, Bluto, crt.sh, etc.
    * Might find for example dev servers that were not initially disclosed.
* Fingerprinting: Nmap, Wappalyzer, WhatWeb, BuiltWith, Netcat
    * Pokes and prods a website/server and try to discern what software, how was it built, what firmware by comparing responses against known data.
* Data breaches: HaveIBeenPwned and similar lists 

#### Scanning & Enumeration

Nmap, Nessus, Nikto, metasploit, what ports to look our for.

#### Gaining access
**TODO**
#### Maintaining access
**TODO**
#### Covering tracks

PS exec is a tool that auto covers your tracks when you leave. In general you don't want to leave malware and logs behind.

### Passive Open source intelligence (OSINT) tools

OSINT is intellegence work using open sources, i.e. sources that are public and completely legal to parse. One way to find an ethically defensible pracitce taret is to go to https://www.bugcrowd.com/products/bug-bounty/, where companies put up bounties for bug finders. Some tools

* hunter.io
    * Go here to get e-mail adresses on corporate domains, common mail patterns, some titles. 
    * Allows us to do enumeration - if we know the company uses for example outlook, we can actually find out what e-mails exist. Outlook has a longer delay before giving you the failed login if you enter an adress that doesn't exist. 

* https://github.com/philipperemy/tensorflow-1.4-billion-password-analysis is a 44gig repo with A LOT of cleartext passwords. These can be analyzed to discover patterns about how people choose passwords, how they change them over time, et cetera. 
    * One simple way to parse it is to look for all @domain adresses and store them in a text file. You can easily see if your target company is a part of this leak. Course giver mentions that some companies he's pentested has hundreds of passwords on this github.
    * We can use this data to inform *data spraying* which is where we jsut try random passwords. This is especially effective if the target is not rate-limiting access attempts.
    * One way of judging whether the obtained credentials are stale is comparing to the pattern at hunter.io - example patterns are firstNameInitial.LastName@domain. 

* Physical recon - check out the site
    * Google maps satellite images, check for nearby food places (because employees will go there, with keycards), security checkpoints, car parks. **NOTE** Googlemaps has timestamps, check how recent your information is. 
    * Google does not update that fast. Some satellite images are pay to play and may be more to date.
    * You can try googling the adress of an office + satellite image and pray. 

* People - hunter.io gave you some e-mail adresses.
    * The easiest targets are poeple with rare names- googling them is more likely to actually turn up the person who is employed at your target company. 
    * Linkedin is great, can confirm workplaces for you.

* Nonbasic google searching:
    * site:\<target\>.com will give you only results from the target website
    * -www filters away adresses that contain www
    * filetype:pdf (or docx, pptx) are interesting, especially if the company has bad access control. 

* **theHarvester** is a tool that finds domains, e-mails under some domain (i.e. tesla.com) by harvesting information from google searches, linkedin, hunter.io et cetera.

* https://haveibeenpwned.com/ is a website that tells you whether a given e-mail has been compromised in any information leaks. This is incredibly useful. 

* there is a tool called Bluto, you install it with pip3. Bluto queries the target domain for some records to get information for subdomains, it will also try to bruteforce subdomains (try out subdomains until one sitcks), check for Wildcard subdomains (this is a DNS domain that will match even on nonexistent subdomains). If the target does not wildcard subdomains, those subdomains are not secured and could in principle bve spoofed. **IMPORTANT** Bluto is active, it generates noise, confirm that your use of this tool is legal before deploying it. 

* crt.sh is a more modern way to find subdomains. Basically, crt looks through DNS certificates (instead of pinging the arget servers?). 

* there is somethig called netcraft for subdomain finding

* Wappalyzer - a tool that identifies what tools are used to run a webside. Detects JavaScript frameworks, cloud hosts, load balances, web server (apache? simpleHTML?), javascript libraries. Contains version information. If you are lucky some of the used library versions have known vulnerabilities. 

* whatweb is a tool that does some fingerprinting. It returned the following about my router:
```
root@6a42d1ad22b0:/usr/src/hackerman# whatweb -v 192.168.1.1
WhatWeb report for http://192.168.1.1
Status    : 200 OK
Title     : NETGEAR Router JWNR2010v5
IP        : 192.168.1.1
Country   : RESERVED, ZZ

Summary   : Script[text/javascript], Frame, MetaGenerator[MSHTML 6.00.2800.1141]

Detected Plugins:
[ Frame ]
        This plugin detects instances of frame and iframe HTML
        elements.


[ MetaGenerator ]
        This plugin identifies meta generator tags and extracts its
        value.

        String       : MSHTML 6.00.2800.1141

[ Script ]
        This plugin detects instances of script HTML elements and
        returns the script language/type.

        String       : text/javascript

HTTP Headers:
        HTTP/1.0 200 OK
        Content-type: text/html
```

* There are websites that let you fingerprint websites without ever having your IP associated to the dubious behaviour. One example is https://builtwith.com/, which gives you a huge swath of information about the target domain. Probably a course in itself to learn how to filter this info. Email hosting providers seems one of the most interesting statistics - for example "proofpoint" is supposedly quite hackable. 

### Active scanning

When port scanning, typically we focus on the TCP side, but there are some important UDP only protocols to have in mind. 

TCP is a connection based protocol. It has a handshake and an acknowledgement loop to ensure that data arrives unscathed. To initiate a handshake, we send out a so-called [SYN] package to request a connection. The client responds with a [SYN, ACK] to indicate the connection is accepted, and the handshake is finalized with a final acknowledgement of the [SYN, ACK] by sending and [ACK].

When we do 'stealth scanning', we do
* [SYN]
* [SYN ACK]
* [RST] (reset)

This means that after we've been told that the target machine has the port on which we tried to establish the connection open, we are done. This method is not particularly good at stealth, since we are sending the recipient our IP in the first SYN message.

For more sophisticated scanning, **nmap** is considered a standard tool. It has a whole suite of ways to scan a target machine. For example, we can do:
* -sS - stealth scan
* -sU - UDP scan

and there is some support for output to text files et cetera. Another thing we can do is give a specific IP adress and run the T4 scanning template, which gives us the following
```
root@6a42d1ad22b0:/usr/src/hackerman# nmap -T4 192.168.1.1
Starting Nmap 7.80 ( https://nmap.org ) at 2020-04-12 11:47 UTC
Nmap scan report for 192.168.1.1
Host is up (0.0025s latency).
Not shown: 996 filtered ports
PORT     STATE SERVICE
23/tcp   open  telnet
53/tcp   open  domain
80/tcp   open  http
5000/tcp open  upnp

Nmap done: 1 IP address (1 host up) scanned in 17.60 seconds
```
We can add some more flags if we want more information. For example, `-A` enables OS detection, version detection, script scanning and traceroute. `-p-` specifies that we want to scan all ports. Adding the -A flag is both very loud (we are sending a lot of requests to the target) and slow. One improvement to just running `nmap -T4 -A -p- 192.168.1.1' right away is so-called staging. In this case, we might split our scanning into

* `nmap -T4 -p- 192.168.1.0`, which returns
```
PORT     STATE SERVICE
23/tcp   open  telnet
53/tcp   open  domain
80/tcp   open  http
5000/tcp open  upnp
```
* Based on this, we already know that ports 23, 54, 80 and 5000 are the open ones, so we might as well do `nmap -T4 -A -p23,53,80,5000 \<ip adress\>`. 
* This can be automated, nmap has the flag -oA that saves scanning results to a file. We could then grep for the list of ports in this file and append them into the nmap -p flag. 
* This can be extended to run your entire suite of favorite scanning tools!

UDP does not have connection semantics, instead it just transfers data. This is good for low latency applicaitons such as real-time voice chat or streaming. This also means there is no handshake, so you would expect to recieve less actionable response. UDP scanning is very unreliable, you get a lot of false positives and they take long. **Nessus** is supposedly pretty good at this, youtube course claims he usually runs this tool then gives up on UDP. Nmap flag for UDP scan is `-sU`

In the directory /usr/share/nmap/scripts, we can find all the scripts that are available to nmap. These scripts try to exploit particular vulnerabilities on the target machine. Use scripts via the flag `--scripts`. I would like to try
`nmap 192.168.1.1 --script=ssl-enum-ciphers -p 80`, but I would need a legal website to target. This script will tell us what SSL ciphers are in use on a particular website, and how hard they are to crack. 

Another tool that we can use is **Nessus**. You need to register for nessus and get an activation code. Nessus is a vulnerability assessment tool that has a large database of known vulnerabilities for various firmware/software/protocols. I should take the time to learn this tool sometime. Check 6h30m in the long youtube video when you have it running some time. 

Another commonly used too is the metasploit framework. This requires you to start a PostgreSQL service on your machine. 
```
service postgresql start
```
You can verify it was started with service --status-all`. We then initialize the metasploit framework
```
root@98f15d02d079:/usr/src/hackerman# msfdb init
System has not been booted with systemd as init system (PID 1). Can't operate.
Failed to connect to bus: Host is down
[+] Starting database
System has not been booted with systemd as init system (PID 1). Can't operate.
Failed to connect to bus: Host is down
[+] Creating database user 'msf'
[+] Creating databases 'msf'
[+] Creating databases 'msf_test'
[+] Creating configuration file '/usr/share/metasploit-framework/config/database.yml'
[+] Creating initial database schema
```
In this case, something is up witht he Docker container, so I am not 100% how this will work. Will return & revise. 

Metasploit has five (ish) module types. Some improtant ones are 
* Auxiliary: scanning, enumeration ("preprocessing" tools)
* exploits: scripts that acutally apply some kind of exploit to a target.

You can search the registry of scripts:
```
msf5 > search portscan

Matching Modules
================

   #  Name                                              Disclosure Date  Rank    Check  Description
   -  ----                                              ---------------  ----    -----  -----------
   0  auxiliary/scanner/http/wordpress_pingback_access                   normal  No     Wordpress Pingback Locator
   1  auxiliary/scanner/natpmp/natpmp_portscan                           normal  No     NAT-PMP External Port Scanner
   2  auxiliary/scanner/portscan/ack                                     normal  No     TCP ACK Firewall Scanner
   3  auxiliary/scanner/portscan/ftpbounce                               normal  No     FTP Bounce Port Scanner
   4  auxiliary/scanner/portscan/syn                                     normal  No     TCP SYN Port Scanner
   5  auxiliary/scanner/portscan/tcp                                     normal  No     TCP Port Scanner
   6  auxiliary/scanner/portscan/xmas                                    normal  No     TCP "XMas" Port Scanner
   7  auxiliary/scanner/sap/sap_router_portscanner                       normal  No     SAPRouter Port Scanner
```
We see here a number of different scanners, which are sorted under the auxiliary set of modules. To use one of them, we do
```
msf5 > use auxiliary/scanner/portscan/syn
msf5 auxiliary(scanner/portscan/syn) > info

       Name: TCP SYN Port Scanner
     Module: auxiliary/scanner/portscan/syn
    License: Metasploit Framework License (BSD)
       Rank: Normal

Provided by:
  kris katterjohn <katterjohn@gmail.com>

Check supported:
  No

Basic options:
  Name       Current Setting  Required  Description
  ----       ---------------  --------  -----------
  BATCHSIZE  256              yes       The number of hosts to scan per set
  DELAY      0                yes       The delay between connections, per thread, in milliseconds
  INTERFACE                   no        The name of the interface
  JITTER     0                yes       The delay jitter factor (maximum value by which to +/- DELAY) in milliseconds.
  PORTS      1-10000          yes       Ports to scan (e.g. 22-25,80,110-900)
  RHOSTS                      yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
  SNAPLEN    65535            yes       The number of bytes to capture
  THREADS    1                yes       The number of concurrent threads (max one per host)
  TIMEOUT    500              yes       The reply read timeout in milliseconds

Description:
  Enumerate open TCP services using a raw SYN scan.
```
We can also inspect and see possible options:
```
msf5 auxiliary(scanner/portscan/syn) > options

Module options (auxiliary/scanner/portscan/syn):

   Name       Current Setting  Required  Description
   ----       ---------------  --------  -----------
   BATCHSIZE  256              yes       The number of hosts to scan per set
   DELAY      0                yes       The delay between connections, per thread, in milliseconds
   INTERFACE                   no        The name of the interface
   JITTER     0                yes       The delay jitter factor (maximum value by which to +/- DELAY) in milliseconds.
   PORTS      1-10000          yes       Ports to scan (e.g. 22-25,80,110-900)
   RHOSTS                      yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
   SNAPLEN    65535            yes       The number of bytes to capture
   THREADS    1                yes       The number of concurrent threads (max one per host)
   TIMEOUT    500              yes       The reply read timeout in milliseconds
```
(note: rhost vs rhosts indicates whether we can give multiple or one port)
We can, after setup, run the exploit
```
msf5 auxiliary(scanner/portscan/syn) > set ports 1-65535
ports => 1-65535
msf5 auxiliary(scanner/portscan/syn) > set rhosts 192.168.1.1
rhosts => 192.168.1.1
msf5 auxiliary(scanner/portscan/syn) > run
SIOCSIFFLAGS: Operation not permitted

[+]  TCP OPEN 192.168.1.1:23
[+]  TCP OPEN 192.168.1.1:53
[+]  TCP OPEN 192.168.1.1:80
```
Hard part with metasploit is knowing what exploit to use. One tip is to search for the name of the service you found on some port.

Another tool is **Nikto**, when used on a domain gives you a lot of information about it and eventual security problems. It tries to offer suggestions on thigns that might be exploitable.

Another tool is **Burpsuite**, which is the state of the art for pentesting web apps. Go back and learn this at some point in the future. The most powerful features are locked behind a pro license. Burpsuite and Nessus are considered the two most important tools you will ever use in a pentesting career.

#### Target practice

A simple way to set up local vulnerable machines is pulling docker images that were made for hacking target practice. STroing this here for reference: https://www.hackingarticles.in/web-application-pentest-lab-setup-using-docker/


### Enumeration

#### An example: Kioptrix Level 1

In the course we will as an example hack & write a report while pwning Kioptrix level 1, which is a prebuilt vulnerable Virtual Machine image. I run Docker for Windows, and my Kali linux is in that docker, so I will only follow along on this example. Hopefully my target practice docker images will help me with some real practice later in or after the course. 

When given a new target, a good step 1 is always Nessus. Fire up Nessus and run a "Basic Network Scan", all ports. Perhaps scan for known web vulnerabilities. 
**Nessus results**:
* A LOT of complaints about outdated versions of OpenSSL.
* 5 cirical vulnerabilities, 37 "high" vulnerabilities and another 100 or so of weaker categories.
* Service detection is an interesting one - although it seems to be the same thing as the standard ping sweep result.
* SSL certificate issues, weak cipher suites, ssl flooding vulnerability. Vulnerable to attacks called "poodle" and "Bar Mitzvah", but these usually require a man in the middle and a lot of time. In general, this is not carried out in a pentest but reported to the customer. 
* `nmap --script=ssl-enum-ciphers -p <port>` will give you a console print version of the Nessus cipher info. This will show most of the ciphers with E-F ratings (to be compared with Tesla's all A rating). Most importantly, cipher of least strength gets an F. This would go in a report. 

Similarly, we run `nmap -T4 -p- \<ip\>` for an initial scan. Note the absence of any advanced scanning. This will find us a few open ports providing some services
![](/images/nmapKioptrix.PNG)

We can now run another nmap with the -A flag on the found ports, with some results:
* We know that the server is running OpenSSH 2.9p2 on port 22
* Port 80 has an Apache httpd 1.3.20 (red-had/Linux mod_ssl/2.8.4)
* Port 111 - open rpc bind
* Port 139 has netbios-ssn smbd service (paired with the above, commonly)
* 443 - same as port 80 ish
* 32768 - RPC bind
* Finds out that the host is named KIOPTRIX. 

On an external webserver, we'd want to look at ports 80 and 443 (the ones with website services). We'd also really want to know why there are netrios and ssh ports exposed. These could sorta be expected on an internal network - since you might want to log in and administrate an eventual webserver. SMB is apparently rife with vulnerabilities that let us execute code. Normally, when there are websites exposed, this is also a good place to attempt remote code execution. This is a capture the flag box, so we know that we are looking to get a root-shell to win, so focusing on the webapp side is unadvisable. 

Next, you could consider actually visiting the website hosted on port 80. IF we go here, we get  a lot of free info
![](/images/openTestPage.PNG)
the box is literally telling us that it is running an apache webserver, and that the operating system is red hat linux! If you were pentesting an actual client, you'd be incredibly worried that this is exposed - if they leave this stuff visible, what else could they be leaving exposed!?. Are they just publishing everything to the internet?

On this kind of machine, with port 80 exposed, Nikto has some specific tools. If we run Nikto it (like everything else) sees a bunch of vulnerabilities - leaking Etagsm no anti.clickjacking headers, exposed apache header, no cross-site-scripting protection.

Open SSL, mod_SSL, Apache versions are vulnerable. The question then is, what exploits are the vulnerable to? How do we find out?

* Allowed HTTP methods: GET, HEAD, OPTION, **TRACE** <- trace is considered potentially dangerous by teh secrutity youtube guy. 

Some directories are accissible (Nikto has done some "dirbusting" for us). Nikto can save to html, more pretty than letting it print to terminal. 

There is a utility called OWASP dirbuster, which just bruteforce looks for visible directories under the website. These can contain a lot of juicy information. Dirbuster has some different directory wordlists that it sources for enumeration. Depending on how much time you have, you can run small, medium or large wordlists. Based on the fact that it is an Apache server, we should look for perhaps *php, asa, sql, zip, tar, pdf, txt, bak*. Note that time taken scales linearly in the number of extensions you look at. 

Dirbuster quickly finds a number of folders. You can access pages that return the html code 200 (SUCCESS) right away while the search is still going. 

In general, with a lot of these results, the most you will get is a reason to question whoever asked you to pentest them why these folders are exposed to the internet at all. 

If we are interested in sending http requests to some IP (for example the test site from before), we can use the tool **curl**, invoked as  `curl --head <IP>`. This will also tell us some information
![](/images/head.PNG)

Another thing to do is to google the precise apache version. Usually you will find results on cvedetails or exploitdb. A lot of DoS vulnerabilities, but also some buffer overflow remote cdoe execution ideas. Exploit-db will sometimes give you actual working code that you can use. Some very spicy OS-dependent hardcoded string in there. Would love to find out how they decided on these...
In this particular case the code is called "OpenFuckV2.c"...

Using searchsploit, it is usually easy to search for know exploits. For example, seraching the exact apache version on this machine (1.3.20), we get some results
```
root@47552e22fec2:/usr/src/hackerman# searchsploit apache 1.3.20
------------------------------------------------------------------------------------ ----------------------------------------
 Exploit Title                                                                      |  Path
                                                                                    | (/usr/share/exploitdb/)
------------------------------------------------------------------------------------ ----------------------------------------
Apache 1.3.20 (Win32) - 'PHP.exe' Remote File Disclosure                            | exploits/windows/remote/21204.txt
Apache 1.3.6/1.3.9/1.3.11/1.3.12/1.3.20 - Root Directory Access                     | exploits/windows/remote/19975.pl
------------------------------------------------------------------------------------ ----------------------------------------
```
But generally, we will miss useful info by doing this - as the target versions of an explot are usually given with ranges. Looking at the major version only, we get
```
root@47552e22fec2:/usr/src/hackerman# searchsploit apache 1.3
------------------------------------------------------------------------------------ ----------------------------------------
 Exploit Title                                                                      |  Path
                                                                                    | (/usr/share/exploitdb/)
------------------------------------------------------------------------------------ ----------------------------------------
Apache 1.0/1.2/1.3 - Server Address Disclosure                                      | exploits/multiple/remote/21067.c
Apache 1.2.5/1.3.1 / UnityMail 2.0 - MIME Header Denial of Service                  | exploits/windows/dos/20272.pl
Apache 1.3 + PHP 3 - File Disclosure                                                | exploits/multiple/remote/20466.txt
Apache 1.3 - Artificially Long Slash Path Directory Listing (1)                     | exploits/multiple/remote/20692.pl
Apache 1.3 - Artificially Long Slash Path Directory Listing (2)                     | exploits/multiple/remote/20693.c
Apache 1.3 - Artificially Long Slash Path Directory Listing (3)                     | exploits/multiple/remote/20694.pl
Apache 1.3 - Artificially Long Slash Path Directory Listing (4)                     | exploits/multiple/remote/20695.pl
Apache 1.3 - Directory Index Disclosure                                             | exploits/multiple/remote/21002.txt
Apache 1.3.12 - WebDAV Directory Listings                                           | exploits/linux/remote/20210.txt
Apache 1.3.14 - Mac File Protection Bypass                                          | exploits/osx/remote/20911.txt
Apache 1.3.20 (Win32) - 'PHP.exe' Remote File Disclosure                            | exploits/windows/remote/21204.txt
Apache 1.3.31 mod_include - Local Buffer Overflow                                   | exploits/linux/local/587.c
Apache 1.3.34/1.3.33 (Ubuntu / Debian) - CGI TTY Privilege Escalation               | exploits/linux/local/3384.c
Apache 1.3.35/2.0.58/2.2.2 - Arbitrary HTTP Request Headers Security                | exploits/linux/remote/28424.txt
Apache 1.3.6/1.3.9/1.3.11/1.3.12/1.3.20 - Root Directory Access                     | exploits/windows/remote/19975.pl
Apache 1.3.x + Tomcat 4.0.x/4.1.x mod_jk - Chunked Encoding Denial of Service       | exploits/unix/dos/22068.pl
Apache 1.3.x - HTDigest Realm Command Line Argument Buffer Overflow (1)             | exploits/unix/remote/25624.c
Apache 1.3.x - HTDigest Realm Command Line Argument Buffer Overflow (2)             | exploits/unix/remote/25625.c
Apache 1.3.x < 2.0.48 mod_userdir - Remote Users Disclosure                         | exploits/linux/remote/132.c
Apache 1.3.x mod_include - Local Buffer Overflow                                    | exploits/linux/local/24694.c
Apache 1.3.x mod_mylo - Remote Code Execution                                       | exploits/multiple/remote/67.c
Apache 1.3/2.0.x - Server Side Include Cross-Site Scripting                         | exploits/multiple/remote/21885.txt
Apache < 1.3.37/2.0.59/2.2.3 mod_rewrite - Remote Overflow                          | exploits/multiple/remote/2237.sh
Apache Archiva 1.0 < 1.3.1 - Cross-Site Request Forgery                             | exploits/multiple/webapps/15710.txt
Apache Archiva 1.3.9 - Multiple Cross-Site Request Forgery Vulnerabilities          | exploits/xml/webapps/40109.txt
Apache Cygwin 1.3.x/2.0.x - Directory Traversal                                     | exploits/windows/remote/23751.txt
Apache Geronimo 2.1.3 - Multiple Directory Traversal Vulnerabilities                | exploits/multiple/remote/8458.txt
Apache Spark Cluster 1.3.x - Arbitrary Code Execution                               | exploits/linux/remote/36562.txt
Apache Struts < 1.3.10 / < 2.3.16.2 - ClassLoader Manipulation Remote Code Executio | exploits/multiple/remote/41690.rb
Apache Win32 1.3.x/2.0.x - Batch File Remote Command Execution                      | exploits/windows/remote/21350.pl
NCSA 1.3/1.4.x/1.5 / Apache HTTPd 0.8.11/0.8.14 - ScriptAlias Source Retrieval      | exploits/multiple/remote/20595.txt
htpasswd Apache 1.3.31 - Local Overflow                                             | exploits/linux/local/466.pl
------------------------------------------------------------------------------------ ----------------------------------------
Shellcodes: No Result
Papers: No Result
```
If we need more information about a given exploit, we can read about it, for example the (win32), apache 1.3.20 specific exploit from the first search has this to tell us
```
root@47552e22fec2:/usr/src/hackerman# cat /usr/share/exploitdb/exploits/windows/remote/21204.txt
source: https://www.securityfocus.com/bid/3786/info

A vulnerability exists in the suggested default configuration for the Apache PHP.EXE binary on Microsoft Windows platforms. This issue has the potential to disclose the contents of arbitrary files to remote attackers.

As a result, it is possible for an attacker to append a filepath to the end of web request for php.exe. Files targetted in this manner will be served to the attacker.

It is also possible to run executables in the PHP directory via successful exploitation of this vulnerability.
```
Holy moly! This would be a bingo if our target was not a redhat linux host. 

Just searching mod_ssl 2.8 in searchsploit, we also find openFuckV2.c
```
root@47552e22fec2:/usr/src/hackerman# searchsploit mod_ssl 2.8
------------------------------------------------------------------------------------ ----------------------------------------
 Exploit Title                                                                      |  Path
                                                                                    | (/usr/share/exploitdb/)
------------------------------------------------------------------------------------ ----------------------------------------
Apache mod_ssl 2.8.x - Off-by-One HTAccess Buffer Overflow                          | exploits/multiple/dos/21575.txt
Apache mod_ssl < 2.8.7 OpenSSL - 'OpenFuck.c' Remote Buffer Overflow                | exploits/unix/remote/21671.c
Apache mod_ssl < 2.8.7 OpenSSL - 'OpenFuckV2.c' Remote Buffer Overflow (1)          | exploits/unix/remote/764.c
Apache mod_ssl < 2.8.7 OpenSSL - 'OpenFuckV2.c' Remote Buffer Overflow (2)          | exploits/unix/remote/47080.c
------------------------------------------------------------------------------------ ----------------------------------------
Shellcodes: No Result
Papers: No Result
```

Even if this is the "jackpot" on this server, we should also attack their ssh protocol (brute-force their ssh keys), mainly because if this was a real evaluation of some customers server, we should see if their users use unsafe passwords and we should check that they actually have a way of detecting such attacks. We will look at such attacks later.

Another vulnerabiltiy of this server is "SMB", where we can actually get information by trying to log in anonymously as root with no password. It will tell us that there are two shares named IPC and ADMIN. The ADMIN share is not accessible without a password, but IPC is. 

Using msfconsole (metasplot framework console), we can do some enumeration on smb. For example, the MSF has a tool that scan for the SNB version (so we can more accurately pick exploits). 

Openfuck supposedly doesn't work that great, and instead we should use openLuck. Find openLuck on google and git clone it. It requires libssl-dev, whicch you might have to apt-get. You then compile the (presumably modified) contained OpenFuck files using gcc.
```
gcc OpenFuck.c -o outfile -lcrypto
```
Then, trying to run the executable with no arguments, you will get some usage instructions. There is some hardcoded offset to tell the program what OS is on the target machine. 
```
./outfile 0x6b <ip to kioptrix> -c 40 
```
And boom, it gives you a shell. You will see that you are root on kioptrix
```
> whoami
root
> hostname
kioptrix.level1
```
When you're in, if this was a real penetration test, it would be time for "post-exploitation" - what *can* we do with the shell we obtained? cat /etc/shadow would give us some hashes that we could crack for password. 

Another vulnerability we can explore is this SMB thing. We fire up the MSF console and search
```
msf5 > search smb_version

Matching Modules
================

   #  Name                               Disclosure Date  Rank    Check  Description
   -  ----                               ---------------  ----    -----  -----------
   0  auxiliary/scanner/smb/smb_version                   normal  No     SMB Version Detection


msf5 > use auxiliary/scanner/smb/smb_version
msf5 auxiliary(scanner/smb/smb_version) > info

       Name: SMB Version Detection
     Module: auxiliary/scanner/smb/smb_version
    License: Metasploit Framework License (BSD)
       Rank: Normal

Provided by:
  hdm <x@hdm.io>

Check supported:
  No

Basic options:
  Name       Current Setting  Required  Description
  ----       ---------------  --------  -----------
  RHOSTS                      yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
  SMBDomain  .                no        The Windows domain to use for authentication
  SMBPass                     no        The password for the specified username
  SMBUser                     no        The username to authenticate as
  THREADS    1                yes       The number of concurrent threads (max one per host)

Description:
  Display version information about each system
```
We then display options 
```
msf5 auxiliary(scanner/smb/smb_version) > options

Module options (auxiliary/scanner/smb/smb_version):

   Name       Current Setting  Required  Description
   ----       ---------------  --------  -----------
   RHOSTS                      yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
   SMBDomain  .                no        The Windows domain to use for authentication
   SMBPass                     no        The password for the specified username
   SMBUser                     no        The username to authenticate as
   THREADS    1                yes       The number of concurrent threads (max one per host)
```
and we see that the only strictly necessary option is the target host adress. In kioptrix, setting the correct host adn doing `run` will find us the version number **Samba 2.2.1a**. This version is vulnerable to an exploit called "trans2open" buffer overflow attack (I found this most easily using `searchsploit samba 2.`). 

We can mount this exploit and try to run it. On kioptrix this starts having problems. This can depend on what payload you are moutning (for example, not staging can generate problems at this stage).

#### Cyber mentor enumerates a Hackthebox

Step 1: nmap the ports. Once you have a lost of ports, -A the ports. 

22 + 80 usually means you find an explot to get credentials over http, and then 22 gives you the shell. Sometimes you find "filtered unknown" services. Netmap, telcom are tools that may be appropriate here. 

On this particular box he finds the good ole apache header vulnerability.

Check the page source. Get some free version info. Fins port 60080 is an admin link.

modifying /etc/hosts/? This changes  what local dns servers you use, may have to use it for hackthebox websites to work as intended in your browser. 

### "Breaking" into hackthebox

This is supposed to be legal, hackthebox explicitly asks me to do this. I feel the hardest part of this task is that I do not know Javascript, so actually figuring out how to hack the site will be a challenge. I assume I am not allowed to run the full kali suite of tools on their domain.
![](/images/hackTheBoxInvite.PNG)

As I understand it, we want to bring up (for example) the Google Chrome devtools and do some inspection. The two external tips I recieved for this challenge were: I can run things in the console, ´think simple`, so I knew not to run every tool I had seen up until now on the website. The first interesting thing you find while clicking around the inspector is this tab:
![](/images/consoleTaunt.PNG)
At some point, among the source files I found the file inviteapi.min.js. After some googling, it turns out that min.js is a compressed form of javascript code that looks really ugly and hard to read. The contents were as follows after some beautification by google:
```
eval(function(p, a, c, k, e, d) {
    e = function(c) {
        return c.toString(36)
    }
    ;
    if (!''.replace(/^/, String)) {
        while (c--) {
            d[c.toString(a)] = k[c] || c.toString(a)
        }
        k = [function(e) {
            return d[e]
        }
        ];
        e = function() {
            return '\\w+'
        }
        ;
        c = 1
    }
    ;while (c--) {
        if (k[c]) {
            p = p.replace(new RegExp('\\b' + e(c) + '\\b','g'), k[c])
        }
    }
    return p
}('1 i(4){h 8={"4":4};$.9({a:"7",5:"6",g:8,b:\'/d/e/n\',c:1(0){3.2(0)},f:1(0){3.2(0)}})}1 j(){$.9({a:"7",5:"6",b:\'/d/e/k/l/m\',c:1(0){3.2(0)},f:1(0){3.2(0)}})}', 24, 24, 'response|function|log|console|code|dataType|json|POST|formData|ajax|type|url|success|api|invite|error|data|var|verifyInviteCode|makeInviteCode|how|to|generate|verify'.split('|'), 0, {}))
```
After some googling, the very strange parenthesis at the end seems to be reconstruction information for decompression, and beautifier.io gives me the following:
```
function verifyInviteCode(code) {
    var formData = {
        "code": code
    };
    $.ajax({
        type: "POST",
        dataType: "json",
        data: formData,
        url: '/api/invite/verify',
        success: function(response) {
            console.log(response)
        },
        error: function(response) {
            console.log(response)
        }
    })
}

function makeInviteCode() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/api/invite/how/to/generate',
        success: function(response) {
            console.log(response)
        },
        error: function(response) {
            console.log(response)
        }
    })
}
```
Finally, the internet taught me a final puzzle piece: you can run commands from the "console" tab of the Chrome devTools. 
![](/images/makeInviteCode.PNG)
to decode the "ROT13" cipher, just google ROT13. We are requested to make a "POST" request to "/api/invite/generate". I didn't know what a POST request was, so I googled it. Turns out POST and GET are the standard "upload" and "download" protocols in HTML, as can possible be deduced from the following netlog
![](/images/network.PNG)

Right, so I already know how to manually send a post request to a specific place, we just take the contents of "makeInviteCode" and change the url field. 
```
$.ajax({
        type: "POST",
        dataType: "json",
        url: '/api/invite/generate',
        success: function(response) {
            console.log(response)
        },
        error: function(response) {
            console.log(response)
        }
    })
```
This gives us a new return payload

![](/images/code.PNG)

but this time it won't tell us how it is encoded! I have yet to use "verifyInviteCode", so one thought I had is to try and invoke it, but since javascript is not types I have no idea what data to give it. Copying the $.ajax command and putting in my encoded key for the call leads to a parsing error, so that isn't the solution at the vary least. 

Googling "decode string ends with =" tells me that apparently there is something called base64 encoding where it turns out that this encoding scheme like to pad with '='. This tells me that it is likely (?) possible that what I have recieved is a base-64 encoded login key.
Still getting parse errors from verifyInviteCode though...

I took the bet and tried the code in the browser. It worked! Yay.


### Exploitation

#### Netcat reverse shell

A shell that listens on a port to the shell of a target machine. We opena  port on our end, and we get the target machine to give us data about its shell.

A bind shell is the opposite, here we open a port on the target machine and we connect to it directly. This is at times easier than reverse shells since we don't have to set up port forwarding and stuff on our local router. 

Netcat is usually called `nc` on Kali machines. Standard is to listen to port `4445`, flag `-lvp` to listen, no flag to go for a bind shell. IP and port are necessary arguments. 

#### Staged and non-staged payloads

Non staged payloads are sent all at once, usually larger in size and they don't always work. Example: windows/meterpreter_reverse_tcp. A staged payload is sent in stages - i.e. we would send the meterpreter and the reverse shell in the previous lod one at a time. MSF will indicate staged payloads with more slashes (windows/meterpreter/reverse_tcp).

#### Example: Hackthebox - Lame

* Step 1: nmap -T4 -p- -A 10.10.10.3

Find some services:
* Port 21/tcp: vsftpd 2.3.4 ---- is it vulnerable?
    * google vulnerabilities, searchsploit it
* port 22/tcp: open ssh 4.7p1. Usually on a real pentest one should try to bruteforce this and see if this attack is visible to the client.
* Samba smb on ports 139, 445 version 3.X-4.X and 3.0.20-Debian
    * Check if the server is open to anonymous log in, see if you get free info. Use `help` to see what commands the shell has for you.
* 3632/tcp distccd --- what is this? :P
    * google it, what is it?

The first thing has an exploit
```
root@dbe84f85853f:/usr/src/hackerman# searchsploit vsftpd 2.3
------------------------------------------------------------------------------------ ----------------------------------------
 Exploit Title                                                                      |  Path
                                                                                    | (/usr/share/exploitdb/)
------------------------------------------------------------------------------------ ----------------------------------------
vsftpd 2.3.2 - Denial of Service                                                    | exploits/linux/dos/16270.c
vsftpd 2.3.4 - Backdoor Command Execution (Metasploit)                              | exploits/unix/remote/17491.rb
------------------------------------------------------------------------------------ ----------------------------------------
```
But the exploit actually depends on a malicious backdoor that was put into the vsftpd source code in version 2.3.4. This was likely patched out. 

Next up, this Samba version has another spicy exploit
```
Description:
  This module exploits a command execution vulnerability in Samba
  versions 3.0.20 through 3.0.25rc3 when using the non-default
  "username map script" configuration option. By specifying a username
  containing shell meta characters, attackers can execute arbitrary
  commands. No authentication is needed to exploit this vulnerability
  since this option is used to map usernames prior to authentication!
```
This will give you a root shell on the lame machine.

Here, you can do some post exploit work, like 'cat etc/shadow', 'arp -a', 'route', look for important files. 

#### Credential stuffing

You compromise a server, you find a bucnh of hashes, and you find someone to crack them for you. When you have some credentials in clear text, you will use these credentials on random websites (or if you are ethical, on the websites within the scope of your job). One good way to do credential stuffing is that huge github repo with 1.4 billion accounts and usernames. 

Simlarly, you can just try passwords from a wordlist, or with pure bruteforce. This is called password spraying. 

### Building an active directory lab

AD explitation, LLMNR Poisoning

AD (Active Directory) is the official access management service provided by Microsoft for Windows Servers. You can create local virtual machines with the Windows Server 2016/Windows 10 enterprise operating systems to use as your AD lab. The point of the lab is to hack the server from outside. I can not actually do it since I insist on running Docker instead of VMs... 

One of the first things to do is name your machine, the course opted for the Marvel them, naming the 2016 server HYDRA. Create some users with basic rights and passwords, security questions and so on.

The 2016 server should (after a reboot) give you a server manage popup, here we want to add roles and features. In this lab, role-based and feature based installation. The server will be using Active Directory Domain services, file and storage services. Next, it will want to add group policy management, .NET framework. Beyond this you can stick with the default. At the end of the installation, you will have to promote the server to domain controller. 

After this, we enter Deployment Configuration, add a new forest. It may be named "marval.local". WSelect DNS and Global catalog servers, choose a password. Leave DNS delegation blank. It will generate a netBIOS name, 'MARVEL'. It will generate you some database fodlers. Keep them. From here on do the default. 

After install, log in as MARVEL/administrator. Now we are the domain controller. Go to tools > active directory users & computers. We can inspect the active directory accounts & users. Notably default_account and guest are disabled. Here, we can create new users, we'll have to give them passwords. Set password never expires for your own sanitys sake. 

The WIN10 system can be renamed once it has finally installed & booted. In this example it ends up named THE PUNISHER. You will have to install vmware tools and reboot. On the windows 10 VM, you can join a domain. You will join the domain of your VM server - there is an option called "Join device to Active Directory Domain". You might have to do "change adapter options" in the VM, click properties, Ipv4, and configure the IP of your MARVEL server as the primary DNS server. You might have to flsuh DNS for this to work. After this, joining the active directory domain will work. The user that was set up on the server will be usable now. You will have to restart the Win10 machine. 

Now we have a Win10 machine that has joined as a user in a 2016 Windows server managed by Active Directory, and we can start learning how to do malicious things. 

#### Our first scenario

SMB - one of the most exploitable things we will ever find. 

To create the scenario, open up the Windows server. Go into File and Storage Services->Shares. Here we make a new folder. Can call it hackme, for example. Create a new share of type SMB Share Quick, choose the new hackme folder. Enable access-based enumeration. Do not encrypt. Default settings for permissions are fine. Go back over to the User windows machine. Go to This PC -> Map network drive and add //HYDRA/Hackme. 

We will be exploiting
* LLMNR - Link Local Multicast Name Resolution
* NBT-NS - NetBios Name Service
These two services are used to identify the host when DNS fails. A big flaw is that they utilize the users username and hash (NTLMv2 (sometimes v1)) in the messages that are sent back and forth.

When a victim tries to access a share that does not exist, for example //HYDRA/hackm, it will not work. This is followed by a broadcast to the network asking other computers "Hey, how do I connect to //HYDRA/hackm"?. If we are on the network, we can intercept this and respond that we can connect them if they send us their hash. Once we have their password hash there are several things we can do. One possibility is just bruteforcing their password from the hash. Another thing we can do is LTMN relaying - we just use the hash itself to authanticate against SMB. We can also do password capture somehow. I suppose we will find out PW capture. 

To attack, we install a man-in-the-middle listener called Responder.py using impacket. This attack only works once we have a foot inside the network, but with no priveleges. The goal here is privelege escalation. The first thing to do in this case set up Responder. Once we start running Nessus, Nmap, traffic fires up and we can accidentally cpature a hash or ten. The best time to run Responder is in the morning as people are coming to work and logging in. 
´´´
python Responder.py -I eth0 -rdw
´´´
One experiment one can do is go to your windows user and try going to your Linux machine's IP in File explorer. This will send your username, IP and hash right to Responder. You'll see waht kind of hash it is (NTLMv2). It is often not too hard to point machines at yourself and get their hash. 

Kali comes with a list called "rockyou.txt" of common passwords for bruteforcing. We can then use hashcat on our newly recieved hash. We do
```
    hashcat -m 5600 hash.txt rockyou.txt
```
Where the 5600 is a flag for NTLMv2 in hashcat - you can list all the modes with `hashcat --help`, and you'll ahve to actually make hash.txxt and put your hash in there. Hashcat takes about once second to crack Password1 on a decent GPU. 

Another wordlist that can be good is "realuniq" that is about 15 gigs. There are some better lists as well, but they are usually proprietary. 

Some basic defenses agains this attack are:
* Disabling LLMNR. Select "Turn off multicast name resultion" under Local Computer Policy -> Computer Configuration -> Administrative Templates -> Netwrok -> DNS CLient in the group ploicy editor. 
* Disable NBT-NS.
If you absolutely must use these features_
* Require network access control - if the hacker can not get onto the network this attack can not be performed. 
* Require strong passwords (>12 characters, common words bad, symbols and numbers good)

The user account on our windows domain sets up their home directory as follows:

![](images/wind10scenario.PNG)

This emulates some real corporate behaviour. The Scans folder is set to be shared with accounts on your network (only yourself at this point) - you scan those documents so people can access them! We will also be making this windows user a local administrator for their own machin under
Computer management->Local users and Groups->Groups->Administrators 
This means that this user can access the Scans via SMB. 

Restart the VM.

Once it boots, log in as administrator. Apparently Windows Defender is OP lately, so we wanna disable Windows Defender. Disable Virus & threat detection. Find real-time protection and turn it off. 

Verify the IP adress of your windows machine.

Finally, we can go back to kali. Step 1, install creckmapexec. What crackmapexec does is a "pass the hash/SMB/whatever" attack. The syntax is
```
crackmapexec smb 192.168.202.0/24 -u Administrator -å 'P@ssw0rd' -d MARVEL
```
(here, the password and domain are actually correct, which is not super realistic). This will show you what computers on the IP-set you scanned that you can get a shell from with these credentials. This tool is fantastic when you have cracked credentials on an assessment.

Next, it is time to fire up metasploit. We will `use exploit/windows/smb/psexec`. We will hack the user account (Frank Castle) in the course. Psexec need syou to know the credential of an account. You can also specify what target executable exploit you want. The options are:
* Automatic
* Powershell
* Native uplaod
* MOF upload
Course author has not had much success with Powershell. The poin here is to get a reverse shell from the target machine. In this case, everything but powershell failed to work. The result is as follows:

![](images/loot200507.PNG)

Shell and system bitnesses don't match (x86 vs x64), which is not optimal. The payload can be manually set, but meterpreter doesn't have an x64 shel. What we can do from this position is do 'ps' to check out existing processes. We want to pick a process that is x64, and to `migrate PID`. This might kill the machine (causing it to need a reboot). To find more things you can do with your meterpreter session, do `help`. Some interesting stuff for the future is port-forwarding, where we can use one machine on the internal net to get access to more machines by having it forward us within the internal network. 

One of the first commands to do is `getsystem`, which is one of the basic ways to get priveleges. Another good command to run is `hashdump`, although on this machine it is not great since we are not in a 64 bit meterpreter session. 

One of the first tools that thecybermentor uses is `load incognito`. We will use this tool to do "token impersonation". Tokens are basically temporary authorization keys used by networking protocols. There are two types of tokens:
* delegate tokens
    * Physical logins to the machine
* impersonate tokens
    * When you are using a network drive, using scripts

We can `list_tokens` (by group or user). We then do `impersonate_token MARVEL\\Administrator` and can impersonate the administrator. If we do getuid the shell will think we are the administrator. From here, we can do `shell` and get a shell. If we do `whoami` the system will see us as the domain admin. We don't have full power though, some commands are only possible on the Windows Domain Controller machine, and this is only a user machine. 

Play around with what you can load in the meterpreter shell (enter in `load` and spam tab). Kiwi can retrieve credentials stored on current machine and dump them to your own machine. You you would prefer to run x64 versions of the moduels on this x86 machine, but as said before you have to take the risk of migrating to another process. 

You can background your meterpreter session, you can try using
`use post/multi/recon/local_exploit_suggester`
It will ask for a session (your open meterpreter session), and it will check for known exploits. With some luck it outputs some useful exploits. 

https://security.stackexchange.com/questions/90578/how-does-process-migration-work-in-meterpreter

If you get a 64bit process and manage to dump credentials (hashes). If you are lucky, all of the computers in the system have the same admin account you just dumped a hash for (due to being default). 

One way to use the admin hash is via crackmapexec. Passing the hash around is not guaranteed to work, but the good thing is you don't need to know the password of admin to perform this attack. With easy (short) passwords on the network, these hashes will usually means that the system is pwned. 

We can also use psexec again. Unset smbdomain and set smbuser to Frank (the user on the target machine) and set smbpass to his dumped hash. 

Log into a second windows 10 machine, (or create one if you don't have one). In the tutorial, the account is called peter. On the Domain Controller (the 2016 server) open gpmc.msc. This is a fast path to the group policy management editor. We want to edit default domain policies->computer configuration->policies->windows settings->security settings->local policies->security options. Scroll until you see microsoft network client. Disable "Digitally sign communications". This is a bad move, but something real admins do to make things faster (iirc).

Go back to your second win10 machine. In the tutorial it is renamed to "SPIDERMAN". Go to Network Status->adapter options. Rightclick on eth0, got to properties, double click IPv4. Set the DNS server to your Domain Controller. Go to domain options and join an Active Directory domain. (You'll need to make a domain account for Peter on your server). Restarts won't hurt.

We will set up the users to have some limited admin privileges on multiple machines. 

#### Important readings for n00bs

https://medium.com/@adam.toscher/top-five-ways-i-got-domain-admin-on-your-internal-network-before-lunch-2018-edition-82259ab73aaa

adsecurity.org
blog.harmj0y.net

blog.g0tmi1k.com/2011/08/basic-linux-privelege-escalation <-- god tier linux guide

www.fuzzysecurity.com/tutorials./16.html <--- god tier windows guide

cau, reflector on github

json beautifier

#### Certifications (Again?) Oh well

eLearnSecurity PTX
PentesterAcademy Attacking and Defending Active Directory
PentesterAcademy Red Team Labs
Hack The Box Rastalabs and Offshore