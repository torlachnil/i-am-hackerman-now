import subprocess
import datetime
import re
import argparse

def writeResult(filename, ping):
    with open(filename, "w") as f:
        f.write(f"Start time {datetime.datetime.now()}")
        for result in ping:
            print(result)
            f.write(result)
        f.write(f"End time {datetime.datetime.now()}")

def pingSubnet(subnet):
    for addr in range(1,255):
        # Argument "shell = False" is a workaround for subprocess to work in container.
        yield subprocess.Popen(["ping", f"{subnet}.{addr}", "-c", "1"], stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE)\
                                .stdout.read().decode()

def main(subnet, filename):
    writeResult(filename, pingSubnet(subnet))

def parseArguments():
    parser = argparse.ArgumentParser(usage='%(prog)s [options] <subnet>',
                                    description = 'ip checker',
                                    epilog = "python sweep.py 192.168.1 -f somefile-txt")
    parser.add_argument('subnet', type=str, help = 'the subnet you want to ping')
    parser.add_argument('-f', '--filename', type = str, help = 'The filename')
    args = parser.parse_args()

    if not re.match(r"(\d{1,3}\.\d{1,3}.\d{1,3})", args.subnet) \
        or any(a not in range(1, 255) for a in map(int, args.subnet.split("."))):
            parser.error("This is not a valid subnet")
    
    if " " in args.filename:
        parser.error("Whitespace forbidden in filename")
    
    return args.subnet, args.filename

if __name__ == '__main__':
    main(*parseArguments())