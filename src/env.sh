echo "sourcing env.sh"

alias hackerman='cd /usr/src/hackerman'
alias man='/usr/share/man'
# Retain besh history between containers.
cat /usr/src/hackerman/hist.txt > ~/.bash_history
history -r
alias exit="history | cut -c 8- > /usr/src/hackerman/hist.txt && exit"

mkdir -p /dev/net && \
mknod /dev/net/tun c 10 200 && \
chmod 600 /dev/net/tun